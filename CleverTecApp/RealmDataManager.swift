//
//  RealmDataManager.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/6/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import Foundation
import RealmSwift

class RealmDataManager {
    
    static var realm: Realm {
            do {
                let realm = try Realm()
                return realm
            }
            catch {
                print("Could not access database: " +  error.localizedDescription)
            }
            return self.realm
    }
    
    static func writeIntoRealm(object: Object) {
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(object)
            }
        } catch let error as NSError{
            print("Something went wrong " + error.localizedDescription)
        }
    }
    
    static func getMetaDataFromRealm() -> Results<MetaData> {
        let data = realm.objects(MetaData.self)
        return data
    }
    
    static func getFieldsDataFromRealm() -> Results<FieldModel> {
        let data = realm.objects(FieldModel.self)
        return data
    }
    
    static func getValuesDataFromRealm() -> Results<ValuesModel> {
        let data = realm.objects(ValuesModel.self)
        return data
    }
    
    static func getResultsFromRealm() -> Results<ResultData> {
        let data = realm.objects(ResultData.self)
        return data
    }
    
    static func getHistoryDataFromRealm() -> Results<HistoryData>{
        let data = realm.objects(HistoryData.self)
        return data
    }
}
