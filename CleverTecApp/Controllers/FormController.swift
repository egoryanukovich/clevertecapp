//
//  FormController.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/10/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import UIKit
import RealmSwift

protocol PopUpValuesTableViewControllerDelegate : class {
    func sendValueAt(index:Int)
}

class FormController: UIViewController {
    
    private var formTableView : UITableView!
    
    let realm = RealmDataManager.realm
    
    var metaDataList: Results<MetaData>!
    let getMetaData = GetMetaData()
    
    var resultList: Results<ResultData>!
    let getResults = GetResultData()
    
    let populateHistory = PopulateHistoryData()
    
    var formDict = [String : Any]()
    var historyDict = [String : Any]()
    
    lazy var activityView : ActivityViewController = {
        let acitvitiViewMessage = NSLocalizedString("wait", comment: "")
        let actView : ActivityViewController = ActivityViewController(message: acitvitiViewMessage)
        
        return actView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createTableView()
        createSendFormButton()
        createShowHistoryButton()
        
        
        self.parent?.present(activityView, animated: true, completion: nil)
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        loadMetaData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        formTableView.estimatedRowHeight = 150
        formTableView.rowHeight = UITableViewAutomaticDimension
    }
    //MARK: - UI elements
    func createTableView(){
        formTableView = UITableView()
        formTableView.contentInset = UIEdgeInsetsMake(20.0, 0.0, 0.0, 0.0)
        self.view.addSubview(formTableView)
        
        formTableView.translatesAutoresizingMaskIntoConstraints = false
        
        formTableView.leadingAnchor.constraint(
            equalTo: view.leadingAnchor).isActive = true
        formTableView.trailingAnchor.constraint(
            equalTo: view.trailingAnchor).isActive = true
        formTableView.topAnchor.constraint(
            equalTo: view.topAnchor, constant: 0).isActive = true
        
        formTableView.heightAnchor.constraint(
            equalTo: view.heightAnchor,
            multiplier: 0.4).isActive = true
        
        formTableView.register(NumericCell.self, forCellReuseIdentifier: CellIdentifiers.numericCell)
        formTableView.register(TextCell.self, forCellReuseIdentifier: CellIdentifiers.textCell)
        formTableView.register(ListCell.self, forCellReuseIdentifier: CellIdentifiers.listCell)
        
        formTableView.dataSource = self
    }
    
    func createSendFormButton() {
        let button = UIButton()
        let buttonMessage = NSLocalizedString("Send form", comment: "")
        
        button.backgroundColor = UIColor.darkGray
        button.setTitle(buttonMessage, for: .normal)
        button.addTarget(self, action: #selector(sendForm(_:)), for: .touchUpInside)
        
        self.view.addSubview(button)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.trailingAnchor.constraint(
            equalTo: view.trailingAnchor).isActive = true
        button.bottomAnchor.constraint(
            equalTo: view.bottomAnchor).isActive = true
        button.widthAnchor.constraint(greaterThanOrEqualToConstant: 50).isActive = true
    }
    
    func createShowHistoryButton(){
        let button = UIButton()
        let buttonMessage = NSLocalizedString("Show History", comment: "")
        
        button.backgroundColor = UIColor.darkGray
        button.setTitle(buttonMessage, for: .normal)
        button.addTarget(self, action: #selector(showHistory(_:)), for: .touchUpInside)
        
        self.view.addSubview(button)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        button.bottomAnchor.constraint(
            equalTo: view.bottomAnchor).isActive = true
        button.widthAnchor.constraint(greaterThanOrEqualToConstant: 50).isActive = true
    }
    
    func addAlert(with message: String){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        self.parent?.present(alert, animated: true, completion: nil)
    }
    //MARK:- Loading data
    func loadMetaData() {
        if RealmDataManager.getMetaDataFromRealm().count != 0 {
            try! realm.write {
                realm.delete(RealmDataManager.getValuesDataFromRealm())
                realm.delete(RealmDataManager.getFieldsDataFromRealm())
                realm.delete(RealmDataManager.getMetaDataFromRealm())
            }
        }
        
        guard let url = URL(string: "http://test.clevertec.ru/tt/meta") else { return }
        
        getMetaData.downloadMetaData(url: url, completion: { (success) in
            if success{
                self.metaDataList = RealmDataManager.getMetaDataFromRealm()
                self.formTableView.reloadData()
                self.navigationItem.title = self.metaDataList[0].headerTitle
                self.activityView.dismiss(animated: true, completion: nil)
            }
        })
    }
    //MARK:- Actions
    @objc func popupAction(_ sender: UIButton){
        let vc = ValuesTableController()
        vc.modalPresentationStyle = UIModalPresentationStyle.popover
        vc.preferredContentSize = CGSize(width: 200, height: 100)
        vc.delegate = self
        
        let popvc = vc.popoverPresentationController
        popvc?.delegate = self
        popvc?.permittedArrowDirections = UIPopoverArrowDirection.any
        popvc?.sourceView = sender
        popvc?.sourceRect = sender.bounds
        
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func sendForm(_ sender : UIButton){
        self.present(activityView, animated: true, completion: nil)
        
        populateHistory.populatingHistoryData(with: historyDict)
        
        if RealmDataManager.getResultsFromRealm().count > 0{
            try! self.realm.write {
                self.realm.delete(RealmDataManager.getResultsFromRealm())
            }
        }
        
        getResults.downloadResults(form: formDict) { (success) in
            if success{
                self.resultList = RealmDataManager.getResultsFromRealm()
                
                self.activityView.dismiss(animated: true, completion: {
                    self.addAlert(with: (self.resultList[0].resultString))
                })
            }
        }
    }
    
    @objc func showHistory(_ sender : UIButton){
        let historyVC = HistoryTableController()
        self.navigationController?.pushViewController(historyVC, animated: true)
        
        let backItem = UIBarButtonItem()
        backItem.title = "Form"
        navigationItem.backBarButtonItem = backItem
    }
    
    func refactorNumericValue(_ value : String) -> String {
        var valueString = ""
        for character in value{
            let char = String(character)
            switch char {
            case "0"..."9":
                let number = Int(char)
                valueString += "\(number!)"
            case ".",","," ":
                valueString += "."
            default:
                break
            }
        }
        return valueString
    }
}
//MARK: - UITableViewDataSource
extension FormController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRowsInSection = RealmDataManager.getFieldsDataFromRealm().count
        return numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let textCell : TextCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.textCell) as? TextCell else {return UITableViewCell()}
            textCell.nameLabel.text = metaDataList[0].fields[indexPath.row].fieldTitle
            
            textCell.nameTextField.delegate = self
            textCell.nameTextField.tag = indexPath.row
            return textCell
        case 1:
            guard let numCell : NumericCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.numericCell) as? NumericCell else {return UITableViewCell()}
            numCell.numericLabel.text = metaDataList[0].fields[indexPath.row].fieldTitle
            
            numCell.numericTextField.delegate = self
            numCell.numericTextField.tag = indexPath.row
            
            return numCell
        case 2:
            guard let listCell : ListCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.listCell) as? ListCell else {return UITableViewCell()}
            let field = metaDataList[0].fields[indexPath.row]
            listCell.listNameLabel.text = field.fieldTitle
            
            listCell.selectValueButton.setTitle(field.fieldValues[0].value, for: .normal)
            listCell.selectValueButton.addTarget(self, action: #selector(popupAction(_:)), for: .touchUpInside)
            
            return listCell
        default:
            break
        }
        return UITableViewCell()
    }
}
//MARK: - UIPopoverPresentationControllerDelegate
extension FormController : UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return true
    }
}
//MARK: - PopUpValuesTableViewControllerDelegate
extension FormController : PopUpValuesTableViewControllerDelegate {
    
    func sendValueAt(index: Int) {
        let indexPath = IndexPath(row: 2, section: 0)
        let listCell : ListCell = formTableView.cellForRow(at: indexPath) as! ListCell
        
        guard let title = RealmDataManager.getValuesDataFromRealm()[index].value  else {return }
        listCell.selectValueButton.setTitle(title, for: .normal)
        let listValue = RealmDataManager.getValuesDataFromRealm()[index].name
        
        guard let listName = RealmDataManager.getFieldsDataFromRealm()[indexPath.row].fieldName else {return }
        formDict[listName] = listValue
        historyDict[listName] = title
    }
}
//MARK: - UITextFieldDelegate
extension FormController : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.tag == 0{
            guard let name = RealmDataManager.getFieldsDataFromRealm()[textField.tag].fieldName else {return }
            formDict[name] = textField.text ?? "No name"
            historyDict[name] = textField.text ?? "No name"
        } else{
            guard let name = RealmDataManager.getFieldsDataFromRealm()[textField.tag].fieldName else {return }
            let numericValue = Double(refactorNumericValue(textField.text ?? "\(0.00)"))
            formDict[name] = numericValue
            historyDict[name] = numericValue
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
