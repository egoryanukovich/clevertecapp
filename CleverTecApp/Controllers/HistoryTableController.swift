//
//  HistoryTableController.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/20/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import UIKit
import RealmSwift

class HistoryTableController: UITableViewController {
    
    var historyData: Results<HistoryData>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let titleMessage = NSLocalizedString("History", comment: "")
        self.navigationItem.title = titleMessage
        
        tableView.contentInset = UIEdgeInsetsMake(20.0, 0.0, 0.0, 0.0)
        
        historyInfo()
    }
    
    func historyInfo()  {
        
        historyData = RealmDataManager.getHistoryDataFromRealm()
    }
    
    func formatDate(_ date:Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .long
        dateFormatter.dateFormat = "dd,MM,yyyy hh:mm a"
        let dayString = dateFormatter.string(from: date)
        
        return dayString
    }
    // MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numberOfRowsInSection: Int = 0
        if historyData.count != 0 {
            
            tableView.separatorStyle = .singleLine
            numberOfRowsInSection = historyData.count
            tableView.backgroundView = nil
        } else {
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            
            let labelText = NSLocalizedString("No data", comment: "")
            noDataLabel.text = labelText
            noDataLabel.textColor = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numberOfRowsInSection
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "NewCell")
        let historyObject = historyData[indexPath.row]
        
        let historyText = historyObject.historyText ?? "No name"
        let historyValue = historyObject.historyValue ?? "No value"
        cell.textLabel?.text = historyText + " \(historyObject.historyNumber) " + historyValue
        cell.detailTextLabel?.text = formatDate(historyObject.historyRequestTime)
        
        return cell
    }
    
}
