//
//  ValuesTableController.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/7/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import UIKit

class ValuesTableController: UITableViewController {
    
    weak var delegate : PopUpValuesTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.contentInset = UIEdgeInsetsMake(5.0, 0.0, 0.0, 0.0)
        tableView.register(ValueCell.self, forCellReuseIdentifier: CellIdentifiers.valueCell)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    // MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRowsInSection = RealmDataManager.getValuesDataFromRealm().count
        return numberOfRowsInSection
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell : ValueCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.valueCell, for: indexPath) as? ValueCell else {return UITableViewCell()}
        
        cell.valueLabel.text = RealmDataManager.getValuesDataFromRealm()[indexPath.row].value
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.sendValueAt(index: indexPath.row)
        self.dismiss(animated: true, completion: nil)
    }
}
