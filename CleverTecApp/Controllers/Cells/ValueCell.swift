//
//  ValueCell.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/9/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import UIKit

class ValueCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let valueLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.numberOfLines = 0
        
        return label
    }()
    
    func setupViews() {
        addSubview(valueLabel)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-18-[v0]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": valueLabel]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[v0]-5-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": valueLabel]))
    }
}
