//
//  TextCell.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/10/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import UIKit

class TextCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let nameLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.numberOfLines = 0
        
        return label
    }()
    
    let nameTextField : UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        let placeholderText = NSLocalizedString("Please enter your name", comment: "")
        textField.placeholder = placeholderText
        textField.font = UIFont.systemFont(ofSize: 14)
        
        textField.textAlignment = .left
        textField.borderStyle = UITextBorderStyle.roundedRect
        
        textField.keyboardType = UIKeyboardType.asciiCapable
        textField.autocapitalizationType = .words
        textField.returnKeyType = .next
        
        return textField
    }()
    
    func setupViews() {
        addSubview(nameLabel)
        addSubview(nameTextField)
        
        addConstraintsForView()
    }
    
    func addConstraintsForView(){
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-18-[v0(<=200)]-[v1]-25-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": nameLabel, "v1":nameTextField]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v0]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": nameLabel]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[v0]-10-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": nameTextField]))
        
        let center = NSLayoutConstraint(item: nameTextField,
                                        attribute: .centerY,
                                        relatedBy: .equal,
                                        toItem: self,
                                        attribute: .centerY,
                                        multiplier: 1.0,
                                        constant: 0)
        
        self.addConstraint(center)
    }
}
