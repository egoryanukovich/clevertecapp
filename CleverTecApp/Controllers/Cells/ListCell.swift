//
//  ListCell.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/10/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {
    
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let listNameLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.numberOfLines = 0
        
        return label
    }()
    
    let selectValueButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.black, for: .normal)
        
        return button
    }()
    
    func setupViews() {
        addSubview(listNameLabel)
        addSubview(selectValueButton)
        addConstraintsForView()
    }
    
    func addConstraintsForView(){
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-18-[v0(==200)]-8-[v1]-25-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": listNameLabel, "v1":selectValueButton]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v0]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": listNameLabel]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[v0]-10-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": selectValueButton]))
    }
}

