//
//  NumericCell.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/10/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import UIKit

class NumericCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let numericLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.numberOfLines = 0
        
        return label
    }()
    
    let numericTextField : UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        let placeholderText = NSLocalizedString("Please enter value", comment:"")
        textField.placeholder = placeholderText
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.textAlignment = .left
        
        textField.borderStyle = UITextBorderStyle.roundedRect
        textField.keyboardType = UIKeyboardType.numbersAndPunctuation
        textField.returnKeyType = .done
        
        return textField
    }()
    
    func setupViews() {
        addSubview(numericLabel)
        addSubview(numericTextField)
        
        addConstraintsForView()
    }
    
    func addConstraintsForView(){
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-18-[v0(<=200)]-[v1]-25-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": numericLabel, "v1":numericTextField]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v0]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": numericLabel]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[v0]-10-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": numericTextField]))
        
        let top = NSLayoutConstraint(item: numericTextField,
                                     attribute: .centerY,
                                     relatedBy: .equal,
                                     toItem: self,
                                     attribute: .centerY,
                                     multiplier: 1.0,
                                     constant: 0)
        self.addConstraint(top)
    }
}
