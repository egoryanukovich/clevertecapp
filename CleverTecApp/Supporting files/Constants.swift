//
//  Constants.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/20/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import Foundation

struct CellIdentifiers {
    static let valueCell = "valueCell"
    static let numericCell = "numericCell"
    static let textCell = "textCell"
    static let listCell = "listCell"
}
