//
//  MetaDataModel.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/6/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import Foundation
import RealmSwift

class MetaData: Object {
    @objc dynamic var headerTitle : String?
    let fields = List<FieldModel>()
}

class FieldModel: Object {
    @objc dynamic var fieldTitle : String?
    @objc dynamic var fieldName : String?
    let fieldValues = List<ValuesModel>()
}

class ValuesModel: Object {
    @objc dynamic var name : String?
    @objc dynamic var value : String?
}
