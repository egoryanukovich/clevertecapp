//
//  HistoryDataCreating.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/20/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import Foundation

class PopulateHistoryData {
    
    func populatingHistoryData(with form: [String:Any]){
        
        guard let name = form["text"] as? String else { return }
        
        let historyObject = HistoryData()
        historyObject.historyRequestTime = Date()
        historyObject.historyText = name
        
        guard let number = form["numeric"] as? Double  else { return }
        
        historyObject.historyNumber = number
        
        guard let value = form["list"] as? String else { return }
        
        historyObject.historyValue = value
        
        RealmDataManager.writeIntoRealm(object: historyObject)
    }
}
