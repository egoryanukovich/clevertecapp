//
//  ResultDataRequest.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/11/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import Foundation

class GetResultData {
    
    func getDataFrom(form: [String : Any], url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        
        let requestBody = [
            "form" : form
        ]        
        do {
            let requestObject = try JSONSerialization.data(withJSONObject: requestBody)
            
            var request = URLRequest(url: url)
            request.httpBody = requestObject
            request.httpMethod = "POST"
            
            URLSession.shared.dataTask(with: request) { data, response, error in
                completion(data, response, error)
                }.resume()
        } catch {
            completion(nil, nil, error)
        }
        
    }
    
    func downloadResults(form: [String:Any], completion: @escaping (Bool) -> Void) {
        print("Download Started")
        let urlString = "http://test.clevertec.ru/tt/data"
        let url = URL(string:urlString)
        
        getDataFrom(form: form,url: url!) { (data, response, error) in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async {
                do {
                    
                    guard let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject]  else { return }
                    
                    guard let result = json["result"] as? String else { return }
                    let resultObject = ResultData()
                    resultObject.resultString = result
                    RealmDataManager.writeIntoRealm(object: resultObject)
                }
                catch {
                    completion(false)
                }
                print("Download Finished")
                completion(true)
            }
        }
    }
    
}
