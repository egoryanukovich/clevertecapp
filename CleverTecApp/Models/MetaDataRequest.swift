//
//  MetaDataRequest.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/6/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import Foundation
import RealmSwift

class GetMetaData {
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadMetaData(url: URL, completion: @escaping (Bool) -> Void) {
        print("Download Started")
        getDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async {
                do {
                    guard let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject]  else { return }
                    
                    guard let title = json["title"] as? String else { return }
                    
                    let metaDataObject = MetaData()
                    metaDataObject.headerTitle = title
                    
                    guard let locations = json["fields"] as? [[String: AnyObject]] else { return }
                    
                    for location in locations {
                        let metaField = FieldModel()
                        
                        guard let fieldName = location["name"] as? String else { return }
                        guard let fieldTitle = location["title"] as? String else { return }
                        
                        metaField.fieldName = fieldName
                        metaField.fieldTitle = fieldTitle
                        metaDataObject.fields.append(metaField)
                        
                        if let values = location["values"] as? [String: AnyObject]{
                            
                            for (key, value) in values{
                                
                                let metaValue = ValuesModel()
                                metaValue.name = key
                                metaValue.value = value as? String
                                metaField.fieldValues.append(metaValue)
                            }
                        }
                    }
                    RealmDataManager.writeIntoRealm(object: metaDataObject)
                }
                catch {
                    completion(false)
                }
                print("Download Finished")
                completion(true)
            }
        }
    }
}
