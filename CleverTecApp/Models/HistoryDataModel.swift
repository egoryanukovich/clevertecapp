//
//  HistoryDataModel.swift
//  CleverTecApp
//
//  Created by Egor Yanukovich on 12/20/17.
//  Copyright © 2017 Egor Yanukovich. All rights reserved.
//

import Foundation
import RealmSwift

class HistoryData: Object {
    @objc dynamic var historyText : String?
    @objc dynamic var historyNumber = 0.0
    @objc dynamic var historyValue : String?
    
    var historyRequestTime  = Date()
}
